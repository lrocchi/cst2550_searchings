public class ArrayST<K extends Comparable<K>,V>{
    
    private K[] keys;
    private V[] values;
    private int n;

    public ArrayST(){
	final int CAPACITY = 100;
	keys = (K[]) new Comparable[CAPACITY];
	values = (V[]) new Object[CAPACITY];
    }
    
    public int size(){
	return n;
    }

    public boolean isEmpty(){
	return n == 0;
    }
    
    public V get(K key){
	if(isEmpty())
	    return null;
	int i = rank(key);
	if(i < n && keys[i].compareTo(key) == 0)
	    return values[i];
	else
	    return null;
    }

    public void put(K key, V value){
	int i = rank(key);
	if(i < n && keys[i].compareTo(key) == 0){
	    values[i] = value;
	    return;
	}

	if(n == keys.length)
	    resize(2*keys.length);
      
      
	for(int j = n; j > i; --j){
	    keys[j] = keys[j-1];
	    values[j] = values[j-1];
	}

	keys[i] = key;
	values[i] = value;
	++n;
    }

    public void resize(int length){

	K[] tempK = (K[]) new Comparable[length];
	V[] tempV = (V[]) new Object[length];

	for(int i = 0; i < n; ++i){
	    tempK[i] = keys[i];
	    tempV[i] = values[i];
	}

	keys = tempK;
	values = tempV;
    }
    

    public boolean contains(K key){
	return get(key) != null;
    }

    public void delete(K key){
	int i = rank(key);
	if(i < n && keys[i].compareTo(key) == 0)
	    keys[i] = null;
    }

    /*
      rank: get the correct index for the given key 
      (whether it is in the ST or not)
    */
    public int rank(K key){
	int lo = 0;
	int hi = n-1;

	while(lo <= hi){
	    int mid = lo + (hi - lo)/2;
	    int cmp = key.compareTo(keys[mid]);
	    if(cmp < 0)
		hi = mid - 1;
	    else if(cmp > 0)
		lo = mid + 1;
	    else
		return mid;
	}
	return lo;
    }

    public Iterable<K> keys(){
	Queue<K> keyQueue = new Queue<K>();
	for(int i = 0; i < n; ++i)
	    keyQueue.enqueue(keys[i]);
	return keyQueue;
    }
}
